package pl.pmajewski.libgdx.squiggly.box2d;

import lombok.Getter;
import lombok.ToString;
import pl.pmajewski.libgdx.squiggly.models.GameActor;

@Getter
@ToString
public class UserData {

    protected GameActor gameActor;
    protected float width;
    protected float height;

    public UserData(GameActor gameActor, float width, float height) {
        this.gameActor = gameActor;
        this.width = width;
        this.height = height;
    }
}
