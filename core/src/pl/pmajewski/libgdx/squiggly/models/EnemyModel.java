package pl.pmajewski.libgdx.squiggly.models;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.libgdx.squiggly.box2d.UserData;
import pl.pmajewski.libgdx.squiggly.utils.AssetsManager;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

public class EnemyModel extends GameActor {

    private TextureRegion characterTexture;
    private ShapeRenderer shapeRenderer;
    private Vector2 position;

    public EnemyModel(World world, Vector2 position) {
        super(world);
        this.position = position;
        create(world);
        setUpTexture();
        shapeRenderer = new ShapeRenderer();

    }
    private void setUpTexture() {
        Texture texture = AssetsManager.getInstance().getAssetManager().get(GlobalProperties.BALL_ASSETS_ID, Texture.class);
        this.characterTexture = new TextureRegion(texture);
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
//        drawHelper(batch, characterTexture);
        batch.end();

        shapeRenderer.setProjectionMatrix(batch.getProjectionMatrix());
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(Color.RED);
        shapeRenderer.rect(getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1,1, (float) Math.toDegrees(body.getAngle()));

        shapeRenderer.end();
        batch.begin();
    }

    private void create(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.KinematicBody;
        bodyDef.position.set(this.position);
        bodyDef.angle = 45f;

        PolygonShape shape = new PolygonShape();
        shape.setAsBox(GlobalProperties.ENEMY_SIZE/2, GlobalProperties.ENEMY_SIZE/2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.BALL_DENSITY;
        fixtureDef.restitution = 0f;

        UserData userData = new UserData(this, GlobalProperties.ENEMY_SIZE, GlobalProperties.ENEMY_SIZE);
        Body body = world.createBody(bodyDef);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(userData);
        body.setUserData(userData);
        body.resetMassData();

        shape.dispose();
        this.body = body;
    }
}
