package pl.pmajewski.libgdx.squiggly.models.walls;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

import pl.pmajewski.libgdx.squiggly.models.BallModel;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

public class VerticalWallRight extends VerticalWall {

    public VerticalWallRight(World world, BallModel ballModel) {
        super(world, ballModel, new Vector2(GlobalProperties.APP_WIDTH + GlobalProperties.SCENE_HORIZONTAL_WALL_WIDTH/2, ballModel.getY()));
    }
}
