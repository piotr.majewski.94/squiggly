package pl.pmajewski.libgdx.squiggly.models;

import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.scenes.scene2d.Actor;

import lombok.Getter;
import pl.pmajewski.libgdx.squiggly.box2d.UserData;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

public abstract class GameActor extends Actor {

    @Getter
    protected Body body;
    protected Rectangle screenRectangle = new Rectangle();
    private World world;

    public GameActor(World world) {
        this.world = world;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(body.getUserData() != null) {
            updateRectangle();
        }
    }

    private void updateRectangle() {
        UserData userData = (UserData) body.getUserData();

        setX(transformToScreen(body.getPosition().x - userData.getWidth() / 2));
        setY(transformToScreen(body.getPosition().y - userData.getHeight() / 2));
        setWidth(transformToScreen(userData.getWidth()));
        setHeight(transformToScreen(userData.getHeight()));
        setRotation((float) Math.toDegrees(body.getAngle()));
    }

    protected float transformToScreen(float n) {
        return GlobalProperties.WORLD_TO_SCREEN * n;
    }

    public String getDebudData() {
        return ""+this.getClass();
    }

    protected void drawHelper(Batch batch, TextureRegion textureRegion) {
        batch.draw(textureRegion, getX(), getY(), getWidth()/2, getHeight()/2, getWidth(), getHeight(), 1, 1, getRotation());
    }

    @Override
    public boolean remove() {
        world.destroyBody(body);
        return super.remove();
    }
}
