package pl.pmajewski.libgdx.squiggly.models;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;

import lombok.Setter;
import pl.pmajewski.libgdx.squiggly.box2d.UserData;
import pl.pmajewski.libgdx.squiggly.stages.Scenery;
import pl.pmajewski.libgdx.squiggly.utils.AssetsManager;
import pl.pmajewski.libgdx.squiggly.utils.GlobalProperties;

public class BallModel extends GameActor {

    private TextureRegion characterTexture;
    private float ballX;
    @Setter
    private Scenery scenery;
    private float lastEnemyRandomizeTimestamp; // By timestamp I mean y position

    public BallModel(World world, float ballX) {
        super(world);
        this.ballX = ballX;
        create(world);
        setUpTexture();
        lastEnemyRandomizeTimestamp = body.getPosition().y;
    }

    @Override
    public void act(float delta) {
        super.act(delta);

        if(scenery != null) {
            if(body.getPosition().y - lastEnemyRandomizeTimestamp > GlobalProperties.ENEMY_GAP) {
                scenery.randomizeEnemy();
                lastEnemyRandomizeTimestamp = body.getPosition().y;
            }
        }
    }

    @Override
    public void draw(Batch batch, float parentAlpha) {
        super.draw(batch, parentAlpha);
        drawHelper(batch, characterTexture);
    }

    private void setUpTexture() {
        Texture texture = AssetsManager.getInstance().getAssetManager().get(GlobalProperties.BALL_ASSETS_ID, Texture.class);
        this.characterTexture = new TextureRegion(texture);
    }

    @Override
    public String getDebudData() {
        return "["+this.getClass()+"] [position: "+body.getPosition()+"] [linearVelocity: "+body.getLinearVelocity()+"] [angle: "+body.getAngle()+"]";
    }

    public void reverseXVelocity() {
        Vector2 velocity = this.body.getLinearVelocity();
        this.body.setLinearVelocity(new Vector2(velocity.x * -1, velocity.y));
    }

    private void create(World world) {
        BodyDef bodyDef = new BodyDef();
        bodyDef.type = BodyDef.BodyType.DynamicBody;
        bodyDef.position.set(new Vector2(this.ballX, GlobalProperties.BALL_Y ));
        bodyDef.linearVelocity.set(new Vector2(GlobalProperties.BALL_VELOCITY_X, GlobalProperties.BALL_VELOCITY_Y));


        CircleShape shape = new CircleShape();
        shape.setRadius(GlobalProperties.BALL_WIDTH/2/2);

        FixtureDef fixtureDef = new FixtureDef();
        fixtureDef.shape = shape;
        fixtureDef.density = GlobalProperties.BALL_DENSITY;
        fixtureDef.restitution = 0.2f;
        fixtureDef.friction = 0f;

        UserData userData = new UserData(this, GlobalProperties.BALL_WIDTH, GlobalProperties.BALL_HEIGHT);
        Body body = world.createBody(bodyDef);
        body.setLinearVelocity(new Vector2(GlobalProperties.BALL_VELOCITY_X, GlobalProperties.BALL_VELOCITY_Y));
        body.setGravityScale(GlobalProperties.BALL_GRAVITY_SCALE);
        Fixture fixture = body.createFixture(fixtureDef);
        fixture.setUserData(userData);
        body.setUserData(userData);
        body.resetMassData();

        shape.dispose();
        this.body = body;
    }
}
