package pl.pmajewski.libgdx.squiggly.utils;

import com.badlogic.gdx.assets.AssetManager;
import com.badlogic.gdx.graphics.Texture;

import lombok.Getter;
import lombok.Setter;

public class AssetsManager {

    private static AssetsManager instance;
    @Getter
    @Setter
    private AssetManager assetManager;

    public static AssetsManager getInstance() {
        if(instance == null) {
            initialize();
        }

        return instance;
    }

    private static void initialize() {
        AssetsManager.instance = new AssetsManager();

        AssetManager assetManager = new AssetManager();
        assetManager.load(GlobalProperties.BALL_ASSETS_ID, Texture.class);
        assetManager.finishLoading();

        AssetsManager.instance.setAssetManager(assetManager);
    }
}
