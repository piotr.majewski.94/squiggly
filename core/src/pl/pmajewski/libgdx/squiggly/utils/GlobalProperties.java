package pl.pmajewski.libgdx.squiggly.utils;

import com.badlogic.gdx.math.Vector2;

public class GlobalProperties {

    public static final boolean ACTORS_DEBUG = false;

    public static final float APP_WIDTH = 8;
    public static final float APP_HEIGHT = 16;

    public static final float SCENE_HORIZONTAL_WALL_HEIGHT = 20;
    public static final float SCENE_HORIZONTAL_WALL_WIDTH = 0.01f;
    public static final float WALL_DENSITY = 1f;

    public static final Vector2 WORLD_GRAVITY = new Vector2(0, 0);
    public static final float WORLD_TO_SCREEN = 1;
    public static final float TIME_STEP = 1/60f;

    public static final float BALL_X = 5.5f;
    public static final float BALL_Y = 4;
    public static final float BALL_WIDTH = 0.75f; // in Box2d world
    public static final float BALL_HEIGHT = 0.75f; // in Box2d world
    public static final float BALL_VELOCITY_X = 6.5f;
    public static final float BALL_VELOCITY_Y = 4.5f;
    public static final float BALL_GRAVITY_SCALE = 1f;
    public static final float BALL_DENSITY = 1f;
    public static final String BALL_ASSETS_ID = "catwoman.png";

    public static final float ENEMY_SIZE = 1f;
    public static final float ENEMY_GAP = 4.5f;
    public static final float ENEMY_INITIAL_GAP = 6f;
    public static final float ENEMY_MAX_Y_SHIFT = 1f;

}
