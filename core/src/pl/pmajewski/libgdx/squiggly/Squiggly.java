package pl.pmajewski.libgdx.squiggly;

import com.badlogic.gdx.Game;

import pl.pmajewski.libgdx.squiggly.screens.GameScreen;

public class Squiggly extends Game {

	@Override
	public void create () {
		setScreen(new GameScreen());
	}
	
	@Override
	public void dispose () {
	}
}